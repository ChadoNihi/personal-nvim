source ~/.config/nvim/_config/main.vim
source ~/.config/nvim/_config/netrw.vim

source ~/.config/nvim/_plugins/main.vim

source ~/.config/nvim/_config/last-lines.vim
