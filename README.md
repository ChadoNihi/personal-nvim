This is (supposed to be) my [neovim](https://neovim.io/) config.

# Prerequisites 
1. Install [minpac](https://github.com/k-takata/minpac)
2. (Optional) install [Ale](https://github.com/w0rp/ale)-supported linters
